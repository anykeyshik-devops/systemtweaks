#!/bin/sh

#####################################################
#   Install glibc with debug symbols on Arch Linux  #
#                                                   #
#         Make by AnyKeyShik Rarity (c) 2021        #
#####################################################

# Install Dependencies
sudo pacman -S git svn gd lib32-gcc-libs patch make bison fakeroot devtools

# Checkout glibc source
svn checkout --depth=empty svn://svn.archlinux.org/packages
cd packages
svn update glibc
cd glibc/repos/core-x86_64

# Add current locale to locale.gen.txt
grep -v "#" /etc/locale.gen >> locale.gen.txt

# Enable debug build in PKGBUILD
sed -i 's#!strip#debug#' PKGBUILD

# Build glibc and glibc-debug packages
makepkg --skipchecksums --config /usr/share/devtools/makepkg-x86_64.conf

# Install glibc-debug
sudo pacman -U *.pkg.tar.xz

sed '/^OPTIONS/ s/!debug/debug/g; /^OPTIONS/ s/strip/!strip/g' /etc/makepkg.conf
