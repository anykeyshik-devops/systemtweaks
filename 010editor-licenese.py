#! /usr/bin/env python3

from flask import Flask

app = Flask(__name__)

@app.route('/cgibin/010editor_check_license_9b.php')
def activate():
    return '<ss>valid</ss><id>17047</id>'

if __name__ == '__main__':
    app.run(port=80)
